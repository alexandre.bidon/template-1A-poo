# Présentation du dépot

Un projet initialisé avec deux packages, transformers et estimators. Un fichier gitignore et un **main** pour lancer votre code.

Dans le requirements.txt on met généralement les dépendances nécessaire pour faire tourner son projet. Le package pipreqs fait ca pour vous. pour l'installer

```
pip install pipreqs dans un terminal
```

puis

```
pipreqs . --force
```
